package mx.unitec.practica1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

class MainActivity : AppCompatActivity() {

    private val tag = "HolaMundo"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Log.d(tag, msg:"onCreate")
    }

    override fun onStart() {
        super.onStart()
        Log.d(tag, msg:"onStart")
    }

    override fun onResume() {
        super.onResume()
        Log.d(tag, msg: "onResume")
    }

    override fun onPause() {
        super.onPause()
        Log.d(tag, msg: "onPause")
    }

    override fun onStop() {
        super.onStop()
        Log.d(tag, msg: "onStop")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(tag, msg: "onDestroy")
    }
}